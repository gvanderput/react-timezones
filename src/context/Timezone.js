import React from "react";
import { DateTime } from "luxon";

export const localTimezone = DateTime.local().zoneName;

export const TimezoneContext = React.createContext();
