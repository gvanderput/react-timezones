import { useState } from "react";
import { TimezoneContext, localTimezone } from "./Timezone";

function TimezoneProvider({ children }) {
  const [selectedTimezone, setSelectedTimezone] = useState(localTimezone);

  return (
    <TimezoneContext.Provider
      value={{
        selectedTimezone,
        setSelectedTimezone,
      }}
    >
      {children}
    </TimezoneContext.Provider>
  );
}

export default TimezoneProvider;
