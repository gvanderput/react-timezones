import { useContext } from "react";
import { TimezoneContext } from "../context/Timezone";
import { DateTime } from "luxon";

function FormattedDate({ isoString, timezone = null }) {
  const { selectedTimezone } = useContext(TimezoneContext);
  const { timeZoneName, ...format } = DateTime.DATETIME_FULL;
  const timezoneToSet = timezone === null ? selectedTimezone : timezone;
  return (
    <span>
      {DateTime.fromISO(isoString)
        .setZone(timezoneToSet)
        .toLocaleString(format)}
    </span>
  );
}

export default FormattedDate;
