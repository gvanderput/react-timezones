import useLaunches from "../hooks/useLaunches";
import Launch from "./launch/Launch";

function Launches() {
  const { isLoading, launches } = useLaunches();

  return (
    <div>
      {isLoading
        ? "Fetching data..."
        : launches.map((launch) => <Launch key={launch.id} {...launch} />)}
    </div>
  );
}

export default Launches;
