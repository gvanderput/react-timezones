import Selector from "../selector/Selector";
import TimezoneProvider from "../../context/TimezoneProvider";
import Launches from "../Launches";
import "./style.scss";

function App() {
  return (
    <TimezoneProvider>
      <h4>SpaceX Upcoming Launches</h4>
      <Selector />
      <Launches />
    </TimezoneProvider>
  );
}

export default App;
