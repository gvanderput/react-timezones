import { useContext } from "react";
import Select from "react-select-search";
import "./style.css";
import styles from "./style.module.scss";
import { timeZonesNames } from "@vvo/tzdb";
import { TimezoneContext } from "../../context/Timezone";
import icon from "../../world.svg";
import classnames from "classnames";
import { DateTime } from "luxon";

const options = timeZonesNames.map((name) => ({
  name,
  value: name,
}));

function Selector() {
  const { selectedTimezone, setSelectedTimezone } = useContext(TimezoneContext);

  return (
    <div className={styles.row}>
      <div>
        <img className={styles.icon} src={icon} alt="World Globe Icon" />
      </div>
      <div>
        <div>
          <h4>Select a timezone:</h4>
        </div>
        <div>
          <Select
            search
            value={selectedTimezone}
            options={options}
            placeholder="Choose your timezone"
            onChange={(timezone) => setSelectedTimezone(timezone)}
            renderOption={(optionProps, optionData, optionSnapshot) => {
              return (
                <button
                  className={classnames("select-search__option", {
                    "is-selected": optionSnapshot.selected,
                    "is-highlighted": optionSnapshot.highlighted,
                  })}
                  {...optionProps}
                >
                  {optionData.name}
                  <span className={styles.offset}>
                    {DateTime.local().setZone(optionProps.value).toFormat("ZZ")}
                  </span>
                </button>
              );
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default Selector;
