import FormattedDate from "../FormattedDate";
import styles from "./style.module.scss";
import { DateTime } from "luxon";
import { TimezoneContext } from "../../context/Timezone";
import { useContext } from "react";

function Label({ color, children, textColor = "inherit" }) {
  return (
    <span
      className={styles.label}
      style={{
        backgroundColor: color,
        color: textColor,
      }}
    >
      {children}
    </span>
  );
}

function TimezoneHeader({ timezone, prefix }) {
  const offset = DateTime.local().setZone(timezone).toFormat("ZZ");
  return <span>{`${prefix} (${timezone} ${offset})`}</span>;
}

function Launch({ date_utc, date_precision, rocket, flight_number }) {
  const { selectedTimezone } = useContext(TimezoneContext);

  return (
    <div className={styles.row}>
      <h4>{rocket.name}</h4>
      <p>{`Flight number: ${flight_number}`}</p>
      <table>
        <tbody>
          <tr>
            <th>Launch date</th>
            <th>
              <TimezoneHeader timezone="UTC" prefix="Source" />
            </th>
            <th>
              <TimezoneHeader
                timezone={DateTime.local().zoneName}
                prefix="Local"
              />
            </th>
            <th>
              <TimezoneHeader timezone={selectedTimezone} prefix="Selected" />
            </th>
          </tr>
          <tr>
            <td
              className={styles.precision}
            >{`Precision: ${date_precision}`}</td>
            <td>
              <FormattedDate isoString={date_utc} timezone="UTC" />
            </td>
            <td>
              <FormattedDate
                isoString={date_utc}
                timezone={DateTime.local().zoneName}
              />
            </td>
            <td>
              <FormattedDate isoString={date_utc} />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Launch;
