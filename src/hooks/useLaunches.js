import { useEffect, useState } from "react";
import axios from "axios";

function useLaunches() {
  const [launches, setLaunches] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const fetchLaunches = new Promise((resolve) => {
    axios
      .get("https://api.spacexdata.com/v4/rockets")
      .then((response) => {
        return response.data.map(({ id, name, description }) => ({
          id,
          name,
          description,
        }));
      })
      .then((rockets) => {
        axios
          .get("https://api.spacexdata.com/v4/launches/upcoming")
          .then((response) => response.data)
          .then((launches) =>
            resolve(
              launches.map(
                ({ date_utc, date_precision, flight_number, rocket, id }) => ({
                  id,
                  date_utc,
                  date_precision,
                  flight_number,
                  rocket: rockets.find((r) => r.id === rocket),
                })
              )
            )
          );
      });
  });

  useEffect(() => {
    fetchLaunches.then((launches) => {
      setLaunches(launches);
      setIsLoading(false);
    });
  }, []);

  return { isLoading, launches };
}

export default useLaunches;
